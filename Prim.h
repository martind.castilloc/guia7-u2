#include <fstream>
#include <iostream>
#include <cstring>

#ifndef PRIM_H
#define PRIM_H

using namespace std;

class Prim{

    private:
        int **matriz;
        int **Lado;

    public:
        Prim();

        // Crea la matriz inicial.
        void crear_matriz(int cantidad_nodo, string nombres[]);
        void nombre_nodos(int cantidad_nodos, string nombres[]);
        // Crea la matriz prim.
        void crear_matriz_prim(int cantidad_nodo);
        // Aplica algoritmo prim.
        void apl_prim(int cantidad_nodos, string nombres[]);
        
        // Impresiones.
        void imprimir_prim(int cantidad_nodos, string nombres[]);
        void imprimir_grafo(int cantidad_nodos, string nombres[]);

        // Funcion para limpiar la terminal.
        void clear();


};
#endif