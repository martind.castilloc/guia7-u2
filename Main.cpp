#include <fstream>
#include <iostream>

using namespace std;

#include "Prim.h"

//Función para el menu. 
string menu (string opc) {

    cout << "\n";
    cout << "\t     ---------------------------- " << endl;
	cout << "\t     ***** MENÚ DE OPCIONES ***** " << endl;
	cout << "\t     ---------------------------- " << endl;
	cout << "\n  [1]  > Aplicar algoritmo Prim\t" << endl;
    cout << "  [2]  > Visualizar el grafo \t" << endl;
    cout << "  [3]  > Salir del programa\t" << endl;
    cout << "Opción: ";

    cin >> opc;
	cin.ignore();

    return opc;
}

// Funcion principal del programa
int main(int argc, char **argv) {
    // Valida cantidad de parámetros mínimos.
    if (argc<2) {
		cout << "Recuerde que el programa recibe dos parametros de entrada "<<endl;
    }

    // Convierte string a entero && tamano_matriz es una variable int para crear la matriz NxN.
    int cantidad_nodos = atoi(argv[1]);

    // Inicializa las variables para el menu
    string option = "\0";
    string dato = "\0";

    // Crea el objeto dijkstra
    Prim *prim = new Prim();

    // Inicializa un Array del tipo string con nombres de los nodos
    string nombres[cantidad_nodos];

    // Genera la matriz como atributo del objeto prim junto a los nombres y el tamaño
    prim->nombre_nodos(cantidad_nodos, nombres);
    prim->crear_matriz(cantidad_nodos, nombres);
    prim->crear_matriz_prim(cantidad_nodos);

    prim->clear();

    while (option != "3") {

        option = menu(option);

        //Opcion para aplicar el algoritmo de Prim.
        if (option == "1") {
            prim->clear();
            prim->apl_prim(cantidad_nodos, nombres);
        
        //Opción para crear y visualizar el grafo con la librería graphviz.
        }else if (option == "2") {
            prim->clear();
            cout <<"\nCreando grafo respectivo..." << endl;
            prim->imprimir_grafo(cantidad_nodos, nombres);
        }      
    }

    return 0;
}
