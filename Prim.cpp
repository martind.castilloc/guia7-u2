#include <fstream>
#include <iostream>
#define INF 9999999

#include "Prim.h"

using namespace std;

Prim::Prim() {}

void Prim::crear_matriz (int cantidad_nodos, string nombres[]){

    this->matriz = new int*[cantidad_nodos];
    int distancia;

    /* Inicializamos la matriz */
    for (int i=0;i<cantidad_nodos;i++) {
        this->matriz[i] = new int[cantidad_nodos];
    }

    /* Llenar la matriz */
    for (int i=0;i<cantidad_nodos;i++) {
        cout << "\n";
        cout << "Trabajando con el nodo: " << nombres[i];
        cout << "\n";

        for (int j=0; j<cantidad_nodos;j++) {
            if (i != j) {
                cout << "ingrese la distancia al nodo <" << nombres[j] << ">: ";
                cin >> distancia;


                if(distancia == wctype("alpha")){
                    break;
                }
                this->matriz[i][j] = distancia;
            } else {
                /* Diagonal de la matriz */
                this->matriz[i][j] = 0;
            
            } 
        }
    }
}

void Prim::nombre_nodos (int cantidad_nodos, string nombres[]){
    string nombre;

    for (int i=0;i<cantidad_nodos;i++) {
        cout << "\n";
        cout << "Ingrese el nombre del nodo " << i+1 << ": ";
        cin >> nombre;
        nombres[i] = nombre;
    }
}

/* Genera y muestra apartir de una matriz bidimensional de enteros,
 el grafo correspondiente.*/
void Prim::imprimir_grafo(int cantidad_nodos, string nombres[]) {

    ofstream fp;
            
    // Abre archivo.
    fp.open ("grafo.txt");
    fp << "digraph G {" << endl;
    fp << "graph [rankdir=LR]" << endl;
    fp << "node [style=filled fillcolor=yellow];" << endl;
    
    // Escribe en el archivo la matriz.
    for (int i=0; i<cantidad_nodos; i++) {
        for (int j=0; j<cantidad_nodos; j++) {
            // evalua la diagonal principal.
            if (i != j) {
                if (matriz[i][j] > 0) {
                    fp << "\n" << '"' << nombres[i] << '"' << "->" << '"' << nombres[j] << '"' << " [label=" << this->matriz[i][j] << "];";
                }
            }
        }
    }
    fp << "}" << endl;

    // Cierra archivo.
    fp.close();
                    
    // Genera el grafo. 
    system("dot -Tpng -ografo.png grafo.txt &");
            
    // Visualiza el grafo. 
    system("eog grafo.png &");
}

void Prim::imprimir_prim(int cantidad_nodos, string nombres[]) {

    ofstream fp;
            
    // Abre archivo.
    fp.open ("grafo_prim.txt");
    fp << "digraph G {" << endl;
    fp << "graph [rankdir=LR]" << endl;
    fp << "node [style=filled fillcolor=green];" << endl;
    
    // Escribir en el archivo el conjunto Lado.
    for (int i=0; i<cantidad_nodos; i++) {
        for (int j=0; j<cantidad_nodos; j++) {
            // evalua la diagonal principal.
            if (i != j) {
                // ignora las conexiones iguales o menores a 0.
                if (this->Lado[i][j] > 0) {
                    fp << "\n" << '"' << nombres[i] << '"' << "->" << '"' << nombres[j] << '"' << " [label=" << this->Lado[i][j] << "];";
                }
            }
        }
    }
    fp << "}" << endl;

    // Cierra archivo. 
    fp.close();
                    
    // Genera el grafo. 
    system("dot -Tpng -ografo_prim.png grafo_prim.txt &");
            
    // Visualiza el grafo. 
    system("eog grafo_prim.png &");
}

void Prim::crear_matriz_prim(int cantidad_nodos) {
    this->Lado = new int*[cantidad_nodos];
    int distancia = 0;

    // Inicializamos el conjunto Lado. 
    for (int i=0;i<cantidad_nodos;i++) {
        this->Lado[i] = new int[cantidad_nodos];
    }

    // Llenar el conjunto Lado con ceros.
    for (int i=0;i<cantidad_nodos;i++) {
        for (int j=0; j<cantidad_nodos;j++) {
                this->Lado[i][j] = distancia;
        }
    }
}

// Función central que aplica el algoritmo de Prim.
void Prim::apl_prim(int cantidad_nodos, string nombres[]) {
  
    /* crea una matriz para rastrear el vértice selected, y
      el selected se convertirá en verdadero o de lo contrario falso */
    int selected[cantidad_nodos];

    // Establece el selected como falso inicialmente.
    memset(selected, false, sizeof(selected));

    // selected se hace verdadero.
    selected[0] = true;
    
    cout << "Arista"
     << " : "
     << "Peso";
    cout << endl;
    int n_aristas = 0;
    int x;  //  numero de fila.
    int y;  //  numero de columna.
    while (n_aristas < cantidad_nodos - 1) {

    int min = INF;
    x = 0;
    y = 0;
    for (int i = 0; i < cantidad_nodos; i++) {
        if (selected[i]) {
            for (int j = 0; j < cantidad_nodos; j++) {
                if (min > this->matriz[i][j]) { 
                    if (!selected[j] && this->matriz[i][j]) {
                        min = this->matriz[i][j];
                        x = i;
                        y = j;
                    }
                }
            }
        }
    }
    this->Lado[x][y] = this->matriz[x][y];
        cout<<" "<< x <<"-"<<y<<":"<<this->matriz[x][y]<<endl;
        selected[y] = true;
        n_aristas++;
    }
    cout <<"\nCreando grafo respectivo..." << endl;
    imprimir_prim(cantidad_nodos, nombres);
}

void Prim::clear() {
    cout << "\x1B[2J\x1B[H";
}